# polygon_grid


## A tool to subdivide polygons into smaller polygons based on Voronoi tesselation.

### *voronoi_splitter* function

The main function of the script is the voronoi_splitter function.

The arguments of the function voronoi_splitter are:
- Input polygon as a geopandas object
- Number of sub-polygons for the output (the number of smaller polygons requested as an output)
- CRS (coordinate reference system) of the input
- Number of random points used to build the Voronoi's polygons, the bigger the number the less variance in the areas of the resulting polygons (but also a big number of point can make the function slow, and eventually break the procces)
- The seed is just for make reproductible the random point generation, does't matter in this case and is set by default to 1

### *json_writer* function

Function to write a JSON file of an output of the voronoi_splitter function, with required data field, to make NFTs

The arguments of the function are:
- Output path for the JSON file
- Geopandas object which should be the output of the voronoi_splitter function
- The fields to include in the JSON file

#### Example of the resulting splitted polygon

![alt text](maps/al.jpeg?raw=true)
