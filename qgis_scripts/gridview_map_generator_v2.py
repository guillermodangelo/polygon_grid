from qgis.core import *
import qgis.utils
import os

for id in range(1, 636):
    crs = QgsCoordinateReferenceSystem('EPSG:32718')
    qinst = QgsProject.instance()
    base_path = os.path.join(qinst.homePath())

    exp = '"subpol_id"= {}'.format(id)
    layer_back = qinst.mapLayersByName('001_nft_back')[0]
    layer_front = qinst.mapLayersByName('001_nft_front')[0]
    poly_layers = [layer_back, layer_front]
    [i.setSubsetString(exp) for i in poly_layers]
    
    # gets raster by name
    raster = QgsProject.instance().mapLayersByName('nft_001_bing_raster_utm')[0]

    # clips by vector and saves
    processing.runAndLoadResults("gdal:cliprasterbymasklayer", {
    'INPUT': raster,
    'MASK': layer_back,
    'SOURCE_CRS': crs,
    'TARGET_CRS': crs,
    'ALPHA_BAND': True,
    'CROP_TO_CUTLINE': True,
    'KEEP_RESOLUTION': True,
    'SET_RESOLUTION': False,
    'OUTPUT': os.path.join(base_path, 'rasters/subpol_{}.tif'.format(id))
    })

    # Move Layer
    root = qinst.layerTreeRoot()
    myalayer = root.findLayer(layer_front.id())
    myClone = myalayer.clone()
    parent = myalayer.parent()
    parent.insertChildNode(0, myClone)
    parent.removeChildNode(myalayer)

    # instantiates the print composer
    composerTitle = 'gridview' # Name of the composer
    project = QgsProject.instance()
    projectLayoutManager = project.layoutManager()
    layout = projectLayoutManager.layoutByName(composerTitle)

    # exports
    path = os.path.join(base_path, 'gridview_images/gridview_img_{}.jpg'.format(id))
    exporter = QgsLayoutExporter(layout)
    exporter.exportToImage(path, QgsLayoutExporter.ImageExportSettings())

    # remove memory raster
    clipped_rast = qinst.mapLayersByName('subpol_{}'.format(id))[0]
    qinst.removeMapLayer(clipped_rast)
