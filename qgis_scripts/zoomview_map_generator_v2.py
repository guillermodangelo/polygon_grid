from qgis.core import *
import qgis.utils

for id in range(1, 636):
    qinst = QgsProject.instance()
    exp = '"subpol_id"= {}'.format(id)
    layer_back = qinst.mapLayersByName('001_nft_back')[0]
    layer_front = qinst.mapLayersByName('001_nft_front')[0]
    poly_layers = [layer_back, layer_front]
    [i.setSubsetString(exp) for i in poly_layers]

    # instantiates canvas
    canvas = iface.mapCanvas()

    # zooms to feature
    extent = layer_front.extent()
    canvas.setExtent(extent)

    # sets scale
    canvas.zoomScale(1000)

    # instantiates the print composer
    composerTitle = 'zoomview' # Name of the composer
    project = QgsProject.instance()
    projectLayoutManager = project.layoutManager()
    layout = projectLayoutManager.layoutByName(composerTitle)

    # gets the map
    map = layout.referenceMap()

    # set extent of the map
    map.zoomToExtent(canvas.extent())

    # loads raster
    qgis.utils.iface.setActiveLayer(layer_back)
    base_path = project.homePath()
    raster_path = os.path.join(base_path, "rasters/subpol_{}.tif".format(id))
    iface.addRasterLayer(raster_path, "subpol_{}".format(id))

    # exports
    path = os.path.join(base_path, "zoomview_images/zoomview_img_{}.jpg".format(id))
    exporter = QgsLayoutExporter(layout)
    exporter.exportToImage(path, QgsLayoutExporter.ImageExportSettings())

    # removes raster    # remove memory raster
    clipped_rast = qinst.mapLayersByName('subpol_{}'.format(id))[0]
    qinst.removeMapLayer(clipped_rast)
