import geopandas as gpd
from geopandas import GeoSeries
import numpy as np
from shapely.geometry import Point, LineString
import shapely
from scipy.spatial import Voronoi
from sklearn import cluster

# inputs
crs = input("Enter the CRS:")
path = input('Enter the path to the layer')
npolygons = input('Enter the numbre of polygons')
npoints = input('Enter the number of points')
seed = input('Enter the seed (default is 1)')


def load_data(path):
    "Loads data"
    return gpd.read_file(path)


def random_points(n, geodf, crs, seed=1):
    "Generates random point over a polygon"
    min_x, min_y, max_x, max_y = geodf.total_bounds

    rng = np.random.default_rng(seed=seed)

    xrand = rng.uniform(min_x, max_x, size=n)
    yrand = rng.uniform(min_y, max_y, size=n)
    points = GeoSeries(map(Point, zip(xrand, yrand))).set_crs(crs)
    points.sindex

    inp, res = geodf.sindex.query_bulk(points.geometry, predicate='intersects')
    intersects = np.isin(np.arange(0, len(points)), inp)

    points_over = points[intersects]
    random_points = gpd.GeoDataFrame({'geometry': points_over}, crs=crs)
    random_points.reset_index(drop=True, inplace=True)

    return random_points


def kmeans_cluster_points(gdf, nclusters):
    "Generates k-means N-sized clusters for a point GeoDataFrame."
    _coords = [(i.x, i.y) for i in gdf.geometry]
    model = cluster.KMeans(n_clusters=nclusters, init='k-means++')
    clusters = model.fit_predict(_coords)

    return clusters


def make_voronois(gdf, points, crs, buffer_size=5000):
    "Makes Voronoi's polygons clipped to an input geodataframe"
    # buffer the input geometry
    _geom = gdf.buffer(buffer_size).geometry[0]

    # adds array of coordinates to apply Voronoi
    array_x = np.array(_geom.exterior.coords.xy[0])[:-1]
    array_y = np.array(_geom.exterior.coords.xy[1])[:-1]

    x = points.geometry.x.values
    x = np.append(x, array_x)

    y = points.geometry.y.values
    y = np.append(y, array_y)

    # stack coordinates
    _coords = np.vstack((x, y)).T

    # calculates Voronois
    vor = Voronoi(_coords)

    # retrieve lines, polygonizes, and converts to GeoDataFrame
    rvert = vor.ridge_vertices
    lines = [LineString(vor.vertices[i]) for i in rvert if -1 not in i]
    polys = shapely.ops.polygonize(lines)
    voronois = gpd.GeoDataFrame(geometry=gpd.GeoSeries(polys), crs=crs)
    voronois.sindex

    # clips with input geometry
    try:
        voronois_clipped = gpd.overlay(voronois, gdf)
        voronois_clipped['area'] = voronois_clipped['geometry'].area
        voronois_clipped['id'] = voronois_clipped.index+1
    except Exception as e:
        print('''Voronoi's polygons creation failed''')
        print(e)

    npolys = voronois_clipped.shape[0]
    max_area = round(max(voronois_clipped['area']))
    min_area = round(min(voronois_clipped['area']))
    print('A total of {} polygons where created.'.format(npolys))
    print('Areas range from {} to {} square meters'.format(min_area, max_area))

    return voronois_clipped


def voronoi_splitter(gdf, npolygons, crs, npoints=10000, seed=1):
    "Splits polygon into smaller parts based on Voronoi's Polygons"
    try:
        coords = random_points(npoints, gdf, crs=crs, seed=seed)
    except Exception as e:
        print('Random points generation failed')
        print(e)

    try:
        coords['cluster'] = kmeans_cluster_points(coords, npolygons)
    except Exception as e:
        print('Clusterization failed')
        print(e)

    try:
        coords_clust = coords.dissolve(by='cluster').centroid
    except Exception as e:
        print('Dissolution of clusters failed')
        print(e)

    try:
        resulting_voronois = make_voronois(gdf, coords_clust, crs=crs)
    except Exception as e:
        print('''Creation of Voronoi's polygons failed''')
        print(e)

    return resulting_voronois


poly = load_data(path)

poly.sindex()

r = voronoi_splitter(poly, npolygons=npolygons, crs=crs, npoints=npoints, seed=seed)

output_path = path+'2'

r.to_file(output_path, driver='GeoJSON')
