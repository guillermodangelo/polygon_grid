import geopandas as gpd
import pandas as pd
from shapely.geometry import Polygon, MultiPolygon, LineString, mapping
from shapely.affinity import rotate, translate
import numpy as np
import math


def read_data(path):
    "Reads input data"
    try:
        data = gpd.read_file(path)
        print('Input data loaded successfully')
    except:
        print('Data loading failed')
    return data


def get_angle(geom):
    "Gets the angle of the minimum oriented bounding box of a polygon"
    # gets the minimum oriented bounding box
    mobb = geom.minimum_rotated_rectangle.boundary
    # calcs angle
    coords = [c for c in mobb.coords]
    segments = [LineString([a, b]) for a, b in zip(coords,coords[1:])]
    # gets the longest segment
    longest_segment = max(segments, key=lambda x: x.length)
    # calcs angle
    p1, p2 = [c for c in longest_segment.coords]
    angle = math.degrees(math.atan2(p2[1]-p1[1], p2[0]-p1[0]))
    angle_rot = (90-angle)
    
    return angle_rot


def make_grid(geom, length, width, crs):
    "Makes a grid that covers a input geometry"
    xmin, ymin, xmax, ymax = geom.bounds

    cols = list(np.arange(xmin, xmax + width, width))
    rows = list(np.arange(ymin, ymax + length, length))

    polygons = []
    for x in cols[:-1]:
        for y in rows[:-1]:
            polygons.append(Polygon([(x,y), (x+width, y), (x+width, y+length), (x, y+length)]))

    return polygons


def rotate_grid(geom, angle, anchor_geom):
    "Rotates a geometry grid at a given angle with given polygon to use its centroid as anchor point"
    multi_geom = MultiPolygon(geom)
    rotated = rotate(multi_geom, angle, origin=(anchor_geom.centroid.x, anchor_geom.centroid.y))
    return list(rotated)


def rotate_polygon(geom, angle, anchor_geom):
    "Rotates a geometry at a given angle with given polygon to use its centroid as anchor point"
    rotated = rotate(geom, angle, origin=(anchor_geom.centroid.x, anchor_geom.centroid.y))
    return rotated


def optimal_grid(path, length, width, iterations, meters_gap, crs):
    """
    :path: string
    :length: int or real
    :widht: int or real
    :iterations: int
    :meters_gap: int or real
    :crs: string
    """
    if length < 10 or width < 10:
        raise Exception("The minimum width and length is 10 meters")
    
    if iterations < 2 or iterations > 200:
        raise Exception("The iterations parameter has to be an integer between 2 and 200")
        
    if meters_gap < 0:
        raise Exception("The meters_gap parameter has to be positive")

    # reads data
    poly = read_data(path)
    
    # gets geom in shapely format
    poly_geometry = poly.geometry[0]
    
    # cell size check
    if poly_geometry.area < (length/width)*3:
        raise Exception('''The area of the cells of grid has to be at
        least 3 times bigger than the area of the polygon''')
    
    # calcs angle
    try:
        angle = get_angle(poly_geometry)
        print('Angle of the input geometry: {} grades'.format(round(angle, 2)))
    except:
        print('Angle calculation failed')
    
    # rotates polygon
    try:
        rotated = rotate_polygon(poly_geometry, angle, poly_geometry)
    except:
        print('Rotation of input polygon failed')

    # buffers original polygon
    try:
        buffered_polygon = rotated.buffer(length*4)
    except:
        print('Buffering of rotated input polygon failed.')
        
    # makes grid
    try:
        _grid = make_grid(buffered_polygon, length, width, crs)
    except:
        print('Initial grid creation failed')
        
    try:
        _grid = rotate_grid(_grid, -angle, poly)
        _grid_gdf = gpd.GeoDataFrame({'geometry': _grid}, crs=crs)
    except:
        print('Grid rotation failed')
    
    # generates copies of the grid translated by the meters_gap parameter
    translated = []
                        
    for i in range(-1, iterations):
        _move = i + meters_gap
        _trans = _grid_gdf.geometry.apply(lambda x: translate(x, xoff=_move, yoff=-_move))
        translated.append(_trans)

    # keeps the geometries that are inside the original polygon
    grids = []

    for grid in translated:
        contained_polygons = []
        for i in grid:
            if poly_geometry.contains(i)==True:
                contained_polygons.append(i)
        grids.append(contained_polygons)

    # picks the grid that covers more area of the input polygon
    # and converts it to geodataframe
    try:
        grids_len = [len(i) for i in grids]
        optimal = grids_len.index(max(grids_len))
        grid_optimized = grids[optimal]
        gdf_grid = gpd.GeoDataFrame({'geometry': grid_optimized}, crs=crs)
        # adds an ID
        gdf_grid['id'] = gdf_grid.index+1
    except:
        print('Optimization failed')

    # calculates the covered area
    try:
        dissolved_grid = gdf_grid.dissolve()
        symdiff = gpd.overlay(dissolved_grid, poly, how='symmetric_difference')
        symdiff_area = sum(symdiff.area)
        uncovered_area = (symdiff_area/poly.area*100).values[0]
        covered_area = 100 - uncovered_area
        print("""Covered area: {}%""".format(round(covered_area,2)))
    except:
        print('Calculation of covered area failed')
              
    return gdf_grid


def export_grid_csv(gdf_grid, filepath):
    "Export data to CSV in path"
    try:
        gdf = gdf_grid
        coords = [mapping(i)['coordinates'][0] for i in gdf.geometry.to_crs(4326)]
        gdf['coords'] = coords
    except:
        print('Calculations of coordinates failed')
        
    try:
        gdf[['id', 'coords']].to_csv(filepath, index=False)
        print('CSV exported successfully to: {}'.format(filepath))
    except:
        print('CSV exporting failed')