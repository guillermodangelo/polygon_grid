import geopandas as gpd
from geopandas import GeoSeries
import numpy as np
import pandas as pd
import shapely
from shapely.geometry import Point, LineString, Polygon, mapping
from scipy.spatial import Voronoi
from sklearn import cluster
import alphashape
import matplotlib.pyplot as plt


def random_points(n, geodf, crs, seed=1):
    "Generates random point over a polygon"
    min_x, min_y, max_x, max_y = geodf.total_bounds

    rng = np.random.default_rng(seed=seed)

    xrand = rng.uniform(min_x, max_x, size=n)
    yrand = rng.uniform(min_y, max_y, size=n)
    points = GeoSeries(map(Point, zip(xrand, yrand))).set_crs(crs)
    points.sindex

    inp, res = geodf.sindex.query_bulk(points.geometry, predicate='intersects')
    intersects = np.isin(np.arange(0, len(points)), inp)

    points_over = points[intersects]
    random_points = gpd.GeoDataFrame({'geometry': points_over}, crs=crs)
    random_points.reset_index(drop=True, inplace=True)

    return random_points


def kmeans_cluster_points(gdf, nclusters):
    "Generates k-means N-sized clusters for a point GeoDataFrame."
    _coords = [(i.x, i.y) for i in gdf.geometry]
    model = cluster.KMeans(n_clusters=nclusters, init='k-means++')
    clusters = model.fit_predict(_coords)

    return clusters


def make_voronois(gdf, points, crs, buffer_size=5000, offset_id=0):
    "Makes Voronoi's polygons clipped to an input geodataframe"
    # buffer the input geometry
    _geom = gdf.buffer(buffer_size).geometry[0]

    # adds array of coordinates to apply Voronoi
    array_x = np.array(_geom.exterior.coords.xy[0])[:-1]
    array_y = np.array(_geom.exterior.coords.xy[1])[:-1]

    x = points.geometry.x.values
    x = np.append(x, array_x)

    y = points.geometry.y.values
    y = np.append(y, array_y)

    # stack coordinates
    _coords = np.vstack((x, y)).T

    # calculates Voronois
    vor = Voronoi(_coords)

    # retrieve lines, polygonizes, and converts to GeoDataFrame
    rvert = vor.ridge_vertices
    lines = [LineString(vor.vertices[i]) for i in rvert if -1 not in i]
    polys = shapely.ops.polygonize(lines)
    voronois = gpd.GeoDataFrame(geometry=gpd.GeoSeries(polys), crs=crs)
    voronois.sindex

    # clips with input geometry
    try:
        vc = gpd.overlay(voronois, gdf)
        vc['subpol_id'] = vc.index + 1 + offset_id
        vc['area'] = round(vc['geometry'].area, 1)
        _vcg = vc.geometry.values
        vc.geometry = [shapely.wkb.loads(shapely.wkb.dumps(i, output_dimension=2)) for i in _vcg]
    except Exception as e:
        print('''Voronoi's polygons creation failed''')
        print(e)

    # gets centroid and reprojects to WGS84
    try:
        _centroid = vc.centroid.to_crs(4326)
        vc['centroid_x'] = np.round(_centroid.x.values, 6)
        vc['centroid_y'] = np.round(_centroid.y.values, 6)
        vc.to_crs(4326, inplace=True)
    except Exception as e:
        print('Reprojection to WGS84 failed')
        print(e)

    npolys = vc.shape[0]
    max_area = round(max(vc['area']))
    min_area = round(min(vc['area']))
    mean_area = round(vc['area'].mean())
    print('A total of {} polygons where created.'.format(npolys))
    print('Areas range from {} to {} square meters'.format(min_area, max_area))
    print('The mean area is {} square meters'.format(mean_area))

    return vc


def get_coordinates(gdf):
    "Get coordinates of geometry as a field"
    try:
        _mapping = mapping(gdf)['features']
        tuples = [i['geometry']['coordinates'][0] for i in _mapping]
    except Exception as e:
        print('Extraction of coordinates failed')
        print(e)

    return tuples


def get_centroid_coordinates(gdf):
    "Get coordinates of centroid as tuples"
    try:
        _centroid_x = gdf['centroid_x'].values
        _centroid_y = gdf['centroid_y'].values
        centroid_coords = [(i,j) for i,j in zip(_centroid_x, _centroid_y)]
    except Exception as e:
        print('Extraction of coordinates of centroid failed')
        print(e)

    return centroid_coords


def json_writer(output_path, gdf, columns):
    "Retrieves coordinates and saves as a JSON file"
    _gdf = gdf.copy()
    # add tuples coordinates as a column
    _gdf['coordinates'] = get_coordinates(gdf)
    _gdf['centroid'] = get_centroid_coordinates(gdf)
    try:
        df = pd.DataFrame(_gdf[columns])
        df.to_json(output_path, orient="records", indent=1)
        print('JSON file was writen in {}'.format(output_path))
    except Exception as e:
        print('Export to JSON failed')
        print(e)   


def voronoi_splitter(gdf, npolygons, crs, npoints=10000, seed=1, offset_id=0):
    "Splits polygon into smaller parts based on Voronoi's Polygons"
    try:
        coords = random_points(npoints, gdf, crs=crs, seed=seed)
    except Exception as e:
        print('Random points generation failed')
        print(e)

    try:
        coords['cluster'] = kmeans_cluster_points(coords, npolygons)
    except Exception as e:
        print('Clusterization failed')
        print(e)

    try:
        coords_clust = coords.dissolve(by='cluster').centroid
    except Exception as e:
        print('Dissolution of clusters failed')
        print(e)

    try:
        resulting_voronois = make_voronois(gdf, coords_clust, crs=crs, offset_id=offset_id)
    except Exception as e:
        print('''Creation of Voronoi's polygons failed''')
        print(e)

    return resulting_voronois


def make_points(coordinates_list):
    "Makes shapely points from a list of tuples of coordinates"
    points = [Point(x) for x in coordinates_list]

    return points


def make_polygon(points, convex_hull):
    "Makes a shapely polygon from a list of shapely points"
    polygon = Polygon([[p.x, p.y] for p in points])
    if convex_hull==True:
        polygon=polygon.convex_hull

    return polygon


def make_concave_hull(coordinates):
    "Make a shapely polygon from a list of shapely points, using a concave hull"
    alpha = 0.95 * alphashape.optimizealpha(coordinates)
    hull = alphashape.alphashape(coordinates, alpha)
    
    return hull


def polygon_from_coord(coordinates_list, mode=False, convex_hull=False):
    "Makes a polygon from a list of tuples of coordinates"
    coords = list(set(coordinates_list))
    try:
        points = make_points(coords)
    except Exception as e:
        print('Creation of point from coordinates failed')
        print(e)
        
    if mode=='concave':
        try:
            polygon = make_concave_hull(coords)
        except Exception as e:
            print('Creation of concave polygon from coordinates failed')
            print(e)
    else:   
        try:
            polygon = make_polygon(points, convex_hull)
        except Exception as e:
            print('Creation of polygon from coordinates failed')
            print(e)

    return polygon


def nft_id(df, pol_id, subpol_id):
    "Concatenates pol_id and subpol_id to create NFT id"
    _pol_padded = df[pol_id].astype(str).str.zfill(3)
    _subpol_padded = df[subpol_id].astype(str).str.zfill(4)
    nft_id = '#' + _pol_padded + "-" + _subpol_padded

    return nft_id


def nft_images(df, subpol_col):
    "Creates images path using subpol NFTs ids"
    ids = df[subpol_col]
    grid_path = '/_img/gridview/gridview_img_{}.jpg'
    name_grid = [grid_path.format(i) for i in ids]

    zoom_path = '/_img/zoomview/zoomview_img_{}.jpg'
    name_zoom = [zoom_path.format(i) for i in ids]
    
    
    return [name_grid, name_zoom]


def description(gdf, name, folder):
    path = folder + '/' + name
    area = gdf['area']

    try:
        fig, axs = plt.subplots(1)
        axs.hist(area, bins=30)
        hist_name = path + '.jpg'
        plt.savefig(hist_name)
        print('Histogram was saved to {}'.format(hist_name))
    except Exception as e:
        print(e)
        print('Failed to save histogram')

    try:
        desc = area.describe()
        print(desc)
        desc_name = path + '.csv'
        desc.to_csv(desc_name, header=False, decimal=',', sep=';')
        print('Descriptive statistics were saved to {}'.format(desc_name))
    except Exception as e:
        print(e)
        print('Failed to save descriptive statistics')
        
        
def coord_lister(geom):
    "Converst coordinates to strings"
    coords = list(geom.exterior.coords)
    flip = [(i[1], i[0]) for i in coords]
    string = [str(i).replace("(", "").replace(")", "") for i in flip]
    string_format = '; '.join(string)

    return string_format